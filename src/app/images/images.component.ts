import { Component, OnInit } from '@angular/core';
import { ImageService } from '../shared/image.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css']
})
export class ImagesComponent implements OnInit {
  constructor(private imageService: ImageService, private router: Router) {}
  static get: any;
  static data: any;
  images: any = ImagesComponent.get;
  autoQuery: any;
  searchQuery: any;
  imagesFound = false;
  searching = false;
  footer = false;

  handleSuccess(data) {
    this.imagesFound = true;
    this.images = data.hits;
    console.log(data.hits);

    ImagesComponent.get = this.images;
  }

  handleError(error) {
    console.log(error);
  }

  // go(data) {
  //   this.router.navigate(['image-detail']);
  // }

  onClick(image) {
    ImagesComponent.data = image;
    console.log(image);
  }

  resetElement() {
    this.footer = true;
  }

  searchImages(query: string) {
    this.searching = true;
    return this.imageService
      .getImage(query)
      .subscribe(
        data => this.handleSuccess(data),
        error => this.handleError(error),
        () => (this.searching = false)
      );
  }

  ngOnInit() {}
}
