import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImagesRoutingModule } from './images-routing.module';
import { AppRoutingModule } from '../app-routing.module';
import { FormsModule } from '@angular/forms';
import { ImageDetailComponent } from '../image-detail/image-detail.component';


@NgModule({
  declarations: [ImageDetailComponent],
  imports: [
    CommonModule,
    ImagesRoutingModule,
    AppRoutingModule,
    FormsModule,
  ]
})
export class ImagesModule { }
