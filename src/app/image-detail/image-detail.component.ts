import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ImagesComponent } from '../images/images.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-image-detail',
  templateUrl: './image-detail.component.html',
  styleUrls: ['./image-detail.component.css']
})
export class ImageDetailComponent implements OnInit {
  constructor(private router: Router,
              private location: Location ) { }

  image: any = ImagesComponent.data;
  error: any = ImagesComponent.data;
  getget: any = ImagesComponent.get;

  ngOnInit() {}

  goBack(): void {
    this.location.back();
    ImagesComponent.get = this.getget;

  }
}
