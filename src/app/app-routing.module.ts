import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImagesComponent } from './images/images.component';
import { ImageDetailComponent } from './image-detail/image-detail.component';


const routes: Routes = [
  { path: 'images', pathMatch: 'full', redirectTo: 'images' },
  { path: '', component: ImagesComponent },
  { path: 'image-detail', component: ImageDetailComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [ImagesComponent,
                                  ImageDetailComponent ];
